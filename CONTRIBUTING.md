## How to Contribute!

If you want to contribute to `Medi-Tweets` a very good place to start are the
[issues with the `accepting merge requests`
label](https://gitlab.com/groups/codeforvenezuela/medi-tweets/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=accepting%20merge%20requests).

Another way is to directly talk to project maintainers and existing
contributors in our [`#proj-medi-tweets` Slack
channel](https://codeforvenezuela.slack.com/messages/CHRD3L8BT).

## Code of Conduct

 TODO

## Contribution Flow

Your contribution starts with an issue being assigned to you.

You should fork the project and start working on a separate local branch.

Once you are happy with your contribution you should submit a Merge Request
(MR) to the upstream branch.

The MR will be then reviewed by the project maintainers and merged once all
discussions and issues resolved.

### Workflow Labels

All the labels used in the `Medi-Tweets` project can be found in the [labels
page](https://gitlab.com/groups/codeforvenezuela/medi-tweets/-/labels).

- Type Labels: ~feature, ~bug, ~"feature proposal"
- Priority Labels:  ~P0, ~P1, ~P2, ~P3

Labels are meant to provide descriptive insights about the issues and aid the
project maintainers in organizing the project.

## Issue Tracker

The issue tracker is for ~bugs and ~"feature proposals".

When submitting a new proposal or filing a bug please use the appropriate issue
template. All issues are reviewed by the maintainers before scheduling any
work. If your proposal is accepted the issue will be labelled appropriately and
you can start your normal [contribution flow](#contribution-flow).


## Merge Requests

TODO
