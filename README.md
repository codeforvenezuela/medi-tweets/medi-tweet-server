# MediTweet

MediTweet is the first Tweet Bot for sourcing medicines to help with the
humanitarian crisis in Venezuela. It is part of ongoing efforts to build
effective tools to help Venezuela. See [Code4Venezuela](codeforvenezuela.org)
for more information.

For more information check [@medi_tweet_ve](https://twitter.com/medi_tweet_ve).


## Contributing

See [our contribution guidelines](./CONTRIBUTING.md).

## Initial Setup

### Clone Gitlab repository
First, set up an account on Gitlab. We then recommend setting up an
[SSH Key](https://docs.gitlab.com/ee/ssh/) for your Git interactions. After
that just clone the repo:

```bash
git clone git@gitlab.com:codeforvenezuela/medi-tweets/medi-tweet-server.git
```

### Install Dependencies
We recommend you install all dependencies in a `conda` environment.
The command below will create a `meditweet` environment for you and install
dependencies into it:

```bash
. init_workspace.sh
```

## Setup package for distribution
```bash
python setup.py develop
```

## Local Deployment and Development

Install [Docker Compose](https://docs.docker.com/compose/install/). Contact
maintainers to get `dev` credentials before running.

```bash
docker-compose build
docker-compose up
```

### Install Pre-commit Hooks
```bash
pre-commit install
```

### Code Format
Included in pre-commit hook.
```bash
black .
```

### Lint
Included in pre-commit hook.
```bash
flake8 .
```

### Unit Tests
```bash
python -m unittest
```

### Coverage
```bash
coverage run --source=example_project -m unittest discover -s tests/
coverage report -m
```

## Maintainers
- @aesadde
- @fabix182
- @fedep3
- @jdarchitect
- @ipince
- @ojcastillo
- @rafaelchacon
- @yessika.labrador
