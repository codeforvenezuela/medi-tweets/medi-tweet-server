if ! conda env list | grep -q 'meditweet'; then
    echo "Didn't find a 'meditweet' conda env. Setting one for you."
    conda create -n meditweet python=3.7 pip
fi

echo "Activating meditweet env"
conda activate meditweet

echo "Installing latest dependencies into env"
pip install -r requirements.txt
pip install -r requirements-test.txt

