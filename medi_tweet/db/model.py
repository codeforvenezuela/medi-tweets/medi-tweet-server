from json import dumps

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import orm

db = SQLAlchemy()


class TweetSearchPaginator(db.Model):
    paginator_id = db.Column(db.BigInteger, primary_key=True)
    since_id = db.Column(db.BigInteger(), index=True)

    def __init__(self, paginator_id, since_id):
        self.paginator_id = paginator_id
        self.since_id = since_id

    def __repr__(self):
        return "paginator_id={}, since_id={}".format(self.paginator_id, self.since_id)


class Medicine(db.Model):
    medicine_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=70), index=True)
    components_str = db.Column(db.String(length=100), index=True)  # array of strings
    presentation = db.Column(db.String(length=200))
    code = db.Column(db.String(100))

    def __init__(self, name, components, presentation, code):
        self.name = name
        self.components = components if components else []
        self.components_str = _serialize_array(self.components)
        self.code = code
        self.presentation = presentation

    @orm.reconstructor
    def init_on_load(self):
        self.components = _deserialize_array(self.components_str)

    def __repr__(self):
        return "name={}, components={}".format(self.name, self.components)


class Tweet(db.Model):
    tweet_id = db.Column(db.BigInteger(), index=True, unique=True, primary_key=True)  # required
    # Tweet data
    # TODO: make required fields actually required.
    username = db.Column(db.String(length=50))  # required

    tweet_ts = db.Column(db.DateTime(), index=True)  # required
    raw_tweet = db.Column(db.Text())  # required
    url = db.Column(db.String(length=100))  # required
    hash_tags_str = db.Column(db.String(length=840))  # array of strings
    # Classifier parts
    # TODO: use Enum instead. Possible values are DEMAND, SUPPLY, UNCLASSIFIED.
    category = db.Column(db.String(length=15), index=True)
    components_str = db.Column(db.String(length=300), index=True)  # array of strings
    medicines_str = db.Column(db.String(length=300), index=True)  # array of strings
    # Other
    responded = db.Column(db.Boolean(), index=True)

    def __init__(
        self, username, tweet_id, tweet_ts, raw_tweet, url, hash_tags, category, components, medicines, responded
    ):
        self.username = username
        self.tweet_id = tweet_id
        self.tweet_ts = tweet_ts
        self.raw_tweet = raw_tweet
        self.url = url
        self.hash_tags = hash_tags if hash_tags else []
        self.hash_tags_str = _serialize_array(self.hash_tags)
        self.category = category
        self.components = components if components else []
        self.components_str = _serialize_array(self.components)
        self.medicines = medicines if medicines else []
        self.medicines_str = _serialize_array(self.medicines)
        self.responded = responded

    # Used when loading an object from the db
    @orm.reconstructor
    def init_on_load(self):
        # transform the serialized array fields into arrays
        self.hash_tags = _deserialize_array(self.hash_tags_str)
        self.components = _deserialize_array(self.components_str)
        self.medicines = _deserialize_array(self.medicines_str)

    def _serialize_array(self, array):
        return DELIMITER.join(array)

    def _deserialize_array(self, string):
        return string.split(DELIMITER)

    def __str__(self):
        return "tweet_ts={}, category={}, components={}, medicines={}, responded={}".format(
            self.tweet_ts, self.category, self.components, self.medicines, self.responded
        )

    def __repr__(self):
        return self.__str__()

    def to_json(self):
        """ Returns a JSON with the main result of the clasification
        """
        ret = {}
        ret["category"] = self.category
        ret["components"] = self.components
        ret["medicines"] = self.medicines
        ret["tweet_id"] = self.tweet_id
        ret["responded"] = self.responded

        return dumps(ret)


DELIMITER = ";;"  # should be something that will be uncommon on hash tags.


def _serialize_array(array):
    return DELIMITER.join(array)


def _deserialize_array(string):
    return string.split(DELIMITER)
