from medi_tweet.services.base_service import BaseService

import base64
import json
import requests


class AngosturaProxyService(BaseService):
    """
    Publish payloads to events on Angostura Proxy
    """

    def __init__(self, url):
        super().__init__()
        self.url = url

    def publish(self, type_, payload, version="1"):
        self.logger.debug(f'Attempting to submit {payload} to event "{type_}"')
        encoded_payload = base64.encodestring(json.dumps(payload).encode("utf-8")).decode("utf-8")
        request_body = {"type": type_, "version": version, "payload": encoded_payload}
        headers = {"Content-Type": "application/json"}
        self.logger.debug(f"Encoded payload to submit to angostura {request_body}")
        r = requests.post(self.url, json=request_body, headers=headers)
        self.logger.debug(f"Received {r.status_code} status code from {self.url}")
