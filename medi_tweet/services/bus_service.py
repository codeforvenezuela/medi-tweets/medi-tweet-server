import base64
import json
import os

from google.api_core.exceptions import GoogleAPICallError
from google.cloud import pubsub_v1
from google.oauth2 import service_account

from medi_tweet.services.base_service import BaseService


class GooglePublisherService(BaseService):
    def __init__(self, project_id: str):
        """
        A Class for Publishing Tweets to Google's PubSub service
        """
        super().__init__()

        scope = "https://www.googleapis.com/auth/pubsub"
        json_string = base64.b64decode(os.environ["PUBSUB_JSON_CREDS"])
        info = json.loads(json_string)
        creds = service_account.Credentials.from_service_account_info(info, scopes=(scope,))
        self.publisher = pubsub_v1.PublisherClient(credentials=creds)
        self.project_id = project_id

    def _topic_path(self, topic):
        return self.publisher.topic_path(self.project_id, topic)

    def publish_event(self, topic, data):
        try:
            topic_path = self._topic_path(topic)
            event = self.publisher.publish(topic_path, data=data)
            self.logger.debug("Published Event {} of message ID {} to GooglePubSub.".format(data, event.result()))
        except GoogleAPICallError as e:
            self.logger.error("Error publishing message to event bus: {}".format(e))
