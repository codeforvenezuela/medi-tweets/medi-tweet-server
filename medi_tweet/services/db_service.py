import json
import logging
from datetime import datetime, timedelta

import sqlalchemy
from sqlalchemy.dialects.mysql import insert

from medi_tweet.db import model
from medi_tweet.db.model import db
from medi_tweet.services.base_service import BaseService


class DBService(BaseService):
    def __init__(self):
        super().__init__()

    def merge(self, obj):
        db.session.merge(obj)

    def commit(self):
        db.session.commit()

    def flag_modified(self, obj, key):
        sqlalchemy.orm.attributes.flag_modified(obj, key)

    def get_tweets(self, component, as_json=False):
        response = {}

        # keywords
        medicines = self._get_medicines_for_component(component)
        keywords = set()

        for m in medicines:
            keywords.add(m.name)
        response["keywords"] = list(keywords)

        # tweets
        tweets = self._get_tweets_for_component(component)
        response["users"] = [{"username": t.username, "raw_tweet": t.raw_tweet, "tweet_url": t.url} for t in tweets]

        return json.dumps(response) if as_json else response

    def _get_medicines_for_component(self, component):
        medicines = model.Medicine.query.filter(model.Medicine.components_str.contains(component)).limit(20).all()

        return medicines

    def _get_tweets_for_component(self, component):
        """Returns recent (7 days) tweets that SUPPLY the given component
        """
        cutoff = datetime.utcnow() - timedelta(days=7)
        tweets = (
            model.Tweet.query.filter(
                model.Tweet.tweet_ts > cutoff,
                model.Tweet.category == "SUPPLY",
                model.Tweet.components_str.contains(component),
            )
            .order_by(sqlalchemy.desc(model.Tweet.tweet_ts))
            .limit(10)
            .all()
        )

        return tweets

    def get_tweet_by_id(self, tweet_id):
        results = model.Tweet.query.filter_by(tweet_id=tweet_id).all()

        if len(results) > 0:
            return results[0]
        else:
            return None

    def get_tweet_paginator_by_id(self, paginator_id):
        row = model.TweetSearchPaginator.query.filter_by(paginator_id=paginator_id).first()

        self.logger.info(f"Fetched tweet paginator id: {paginator_id}. Got row: {row} from db")

        if row is not None:
            return row.since_id

        return row

    def update_tweet_paginator_by_id(self, paginator_id, since_id):
        self.logger.info(f"Setting tweet paginator for id: {paginator_id} to since_id: {since_id}")
        insert_stmt = insert(model.TweetSearchPaginator).values(paginator_id=paginator_id, since_id=since_id)
        on_duplicate_key_stmt = insert_stmt.on_duplicate_key_update(since_id=insert_stmt.inserted.since_id)
        db.session.execute(on_duplicate_key_stmt)
        db.session.commit()

    def get_tweets_by_user(self, username):
        return model.Tweet.query.filter_by(username=username).all()

    def get_unresponded_tweets(self, time_delta: int):
        cutoff = datetime.utcnow() - timedelta(days=time_delta)
        results = model.Tweet.query.filter(
            model.Tweet.tweet_ts >= cutoff,
            model.Tweet.responded == False,  # noqa: E712
            model.Tweet.category == "DEMAND",
            model.Tweet.medicines_str != "" or not model.Tweet.medicines_str,
        ).all()

        filtered = list(filter(lambda r: r.medicines != [""], results))
        self.logger.debug(f"Filtered tweets to respond: {filtered}")

        return filtered

    def add_tweet(self, tweet):
        try:
            db.session.add(tweet)
            db.session.commit()
        except Exception as e:
            logging.error(f"Db error {e} -- ROLLING BACK")
            db.session.rollback()
