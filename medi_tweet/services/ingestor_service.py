import json

from medi_tweet.services.base_service import BaseService
from medi_tweet.services.search_service import SearchService


class TweetIngestorService(BaseService):
    def __init__(self, hashtags, db_service, paginator_id, proxy_publisher, hashtag_event):
        super().__init__()
        self.db_service = db_service
        self.proxy_publisher = proxy_publisher
        self.paginator_id = paginator_id
        self.search_service = SearchService()
        self.hashtags = hashtags + [h.lower() for h in hashtags]
        self.hashtag_event = hashtag_event
        self.logger.info(
            f"Service initialized with hashtags: {self.hashtags}. Pushing Angostura messages to event: {self.hashtag_event}"
        )

    def ingest(self):
        # When changing the search query, we should bump the paginator id.
        since_id = self.db_service.get_tweet_paginator_by_id(self.paginator_id)
        raw_tweets, max_id = self.search_service.search(words=[], hashtags=self.hashtags, since_id=since_id)
        self.db_service.update_tweet_paginator_by_id(self.paginator_id, max_id)

        for tweet in raw_tweets:
            self._publish_raw_tweet(tweet)

        return raw_tweets

    def _publish_raw_tweet(self, tweet):
        self.proxy_publisher.publish(
            type_="tweet", payload={"hashtag": self.hashtag_event, "tweet_payload": tweet._json}
        )
