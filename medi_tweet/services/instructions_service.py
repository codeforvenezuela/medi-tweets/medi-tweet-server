import os
import random

from tweepy import TweepError

from medi_tweet.services.base_service import BaseService
from medi_tweet.twitter.api import tweepy_auth


class InstructionTweetsService(BaseService):
    def __init__(self):
        super().__init__()
        self.tweets_filename = os.environ["INSTRUCTION_TWEETS_PATH"]
        self.tweets = self._load_tweets()
        self.last_index = -1
        self.api = tweepy_auth()

        # Debug mode
        self._init_mode()
        self.send_tweet = self._send_tweet if not self.debug else self._debug_tweet

    def _load_tweets(self):
        with open(self.tweets_filename) as file:
            tweets = file.readlines()

        self.logger.info(f"Loaded {len(tweets)} Instruction Tweets")

        return tweets

    def _send_tweet(self, tweet):
        try:
            if tweet != "\n ":
                self.api.update_status(tweet)
        except TweepError as e:
            self.logger.error(e.reason)
            raise e

    def _debug_tweet(self, tweet):
        self.logger.debug(f"This would have been the instruction tweet: {tweet}")

    def tweet_instructions(self):
        tweet_index = -1

        while tweet_index == self.last_index:
            # Avoid Tweeting the same thing twice in a row
            tweet_index = random.randint(0, len(self.tweets) - 1)
        self.last_index = tweet_index

        tweet = self.tweets[tweet_index]
        self.logger.info(f"Tweeting Instruction: {tweet}")
        self.send_tweet(tweet)
