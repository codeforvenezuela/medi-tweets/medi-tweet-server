import pickle
from typing import List

from sklearn.feature_extraction.text import CountVectorizer

from medi_tweet.services.base_service import BaseService
from medi_tweet.services.data_service import DataService


class PredictionService(BaseService):
    def __init__(self, model_file, vocab_file, medicines_csv):
        super().__init__()

        self.data_service = DataService(medicines_csv)

        self.category_map = {0: "UNCLASSIFIED", 1: "SUPPLY", 2: "DEMAND"}

        with open(f"{model_file}", "rb") as f:
            self.model = pickle.load(f)
        self.logger.info("Model File Loaded")

        with open(f"{vocab_file}", "rb") as f:
            self.vocabulary = pickle.load(f)
        self.logger.info("Vocabulary File Loaded")

        self.vectorizer = CountVectorizer(
            strip_accents="unicode",
            analyzer="word",
            tokenizer=None,
            preprocessor=None,
            stop_words=None,
            max_features=100,
            ngram_range=(1, 2),
            binary=True,
            vocabulary=self.vocabulary,
        )

    def _get_components(self, medicines: List[str]) -> List[str]:
        instance_components = set()

        for medicine in medicines:
            if medicine in self.data_service.components.keys():
                for component in self.data_service.components.get(medicine):
                    instance_components.add(component)

        return list(instance_components)

    def predict(self, instances):
        result = []

        for instance in instances:
            raw_tweet = instance["raw_tweet"]
            text = self.data_service.pre_process_text(raw_tweet)
            self.logger.debug(f"Pre-processed text:{text}, for tweet: {raw_tweet}")
            instance_medicines = self.data_service.get_medicines(instance["raw_tweet"].lower())

            vector = self.vectorizer.transform([text]).toarray()
            probabilities = self.model.predict_proba(vector)
            category = self.model.predict(vector).tostring()[0]

            instance["category"] = self.category_map.get(category, -1)
            instance["medicines"] = list(instance_medicines)
            instance["components"] = self._get_components(instance_medicines)
            instance["probabilities"] = probabilities.tolist()

            result.append(instance)

        return result
