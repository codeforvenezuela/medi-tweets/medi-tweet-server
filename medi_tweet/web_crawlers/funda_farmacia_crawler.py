import requests
import re
from bs4 import BeautifulSoup
from medi_tweet.web_crawlers.base_crawler import BaseCrawler


class FundafarmaciaCrawler(BaseCrawler):
    def __init__(self, base_url):
        super().__init__(base_url)

    def crawl(self, medicine):
        try:
            self.logger.debug(f"Crawling for info about medicine:{medicine}")
            source_code = requests.post(self.url, data={"producto": medicine, "estado": "", "lugar": ""}, timeout=30)
        except Exception as e:
            self.logger.error(e)

            return []

        plain_text = source_code.text
        soup = BeautifulSoup(plain_text, "html.parser")
        tds = soup.find_all("td")
        info = self._collect_info(tds)

        return info

    def _collect_info(self, tds):
        # Struct store: {'farmacia': 'Farmarket', 'sede' : nombre, 'productos' : productosPorTienda}
        # Struct products: {'producto' : nombre, 'disponibles' : xxx}

        i = 0
        total_list = []
        products_by_location = []

        while i < len(tds):

            state = re.split(r">|<", str(tds[i + 3]))[2]
            location = re.split(r">|<", str(tds[i + 4]))[2]
            current_location = location

            while i < len(tds) and location == current_location:
                product_regex = re.split(r">|<", str(tds[i]))[2].strip()
                products = {"name": product_regex}
                products_by_location.append(products)

                i += 6

                if i < len(tds):
                    current_location = re.split(r">|<", str(tds[i + 4]))[2]

            store = {"name": "Fundafarmacia", "address": location + ", " + state, "medicines": products_by_location}

            total_list.append(store)

            products_by_location = []

        return total_list
